def leapyear(input_year):
    if (input_year) % 400 == 0:
        return 'True'
    elif (input_year) % 100 == 0:
        return 'False'
    elif (input_year) % 4 == 0:
        return 'True'
    else:
        return 'False'
print leapyear(input())
