# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Tweet(models.Model):
    title = models.CharField(max_length=200)
    date  = models.DateTimeField(auto_now_add=True)
    body  = models.CharField(max_length=1000)
    image = models.BinaryField(blank=True)
    def __str__(self):
        return self.title