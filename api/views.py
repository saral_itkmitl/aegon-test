# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from tweet.models import Tweet
from .serializers import TweetSerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
# class TweetViewSet(ModelViewSet):
#     queryset = Tweet.objects.all()
#     serializer_class = TweetSerializer

def Tweet(request):
    if request.method == 'GET':
        queryset = Tweet.objects.all()
        serializer_data = TweetSerializer(queryset, many=True)
        return Response(serializer_data.data)
    elif request.method == 'POST':
        serializer = TweetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)